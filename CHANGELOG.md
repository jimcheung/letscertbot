# Changelog

## [v0.3.2](https://github.com/jinhucheung/letscertbot/tree/v0.3.2) (2020-02-05)

[Full Changelog](https://github.com/jinhucheung/letscertbot/compare/v0.3.1..v0.3.2)

**Implemented enhancements:**

- Add GoDaddy DNS support [\#12](https://github.com/jinhucheung/letscertbot/pull/12)

**Fixed bugs:**

- Remove dns argument from Renewal script [\#12](https://github.com/jinhucheung/letscertbot/pull/12)

## [v0.3.1](https://github.com/jinhucheung/letscertbot/tree/v0.3.1) (2020-02-03)

[Full Changelog](https://github.com/jinhucheung/letscertbot/compare/v0.3.0..v0.3.1)

**Breaking changes:**

- Change API module to DNS module [\#11](https://github.com/jinhucheung/letscertbot/issues/11)

## [v0.3.0](https://github.com/jinhucheung/letscertbot/tree/v0.3.0) (2020-02-02)

[Full Changelog](https://github.com/jinhucheung/letscertbot/compare/v0.2.0..v0.3.0)

**Breaking changes:**

- Add Docker support [\#8](https://github.com/jinhucheung/letscertbot/pull/8)

**Implemented enhancements:**

- Add QCloud DNS support [\#5](https://github.com/jinhucheung/letscertbot/pull/5)

## [v0.2.0](https://github.com/jinhucheung/letscertbot/tree/v0.2.0) (2019-10-26)

[Full Changelog](https://github.com/jinhucheung/letscertbot/compare/v0.1.0..v0.2.0)

**Breaking changes:**

- Change deployment configuration, remove `deploy.keep_backups` [\#3](https://github.com/jinhucheung/letscertbot/pull/3)

**Fixed bugs:**

- Can't renew certificate if `deploy.server.deploy_to` set `/etc/letsencrypt/live` on local deployment [\#4](https://github.com/jinhucheung/letscertbot/pull/4)

## [v0.1.0](https://github.com/jinhucheung/letscertbot/tree/v0.1.0) (2019-10-23)

**Implemented enhancements:**

- Obtain certificate
- Renewal certificate
- Deploy certificate
- Add Aliyun DNS support