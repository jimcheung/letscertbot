#!/usr/bin/env python
# -*- coding: utf-8 -*-

__all__ = ['Aliyun', 'Qcloud', 'GoDaddy']

from dns.aliyun import Aliyun
from dns.qcloud import Qcloud
from dns.godaddy import GoDaddy